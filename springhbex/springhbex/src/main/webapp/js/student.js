$(document).ready(function(){
	getAllStudents();
	getAllTrainers();
	getAllConsultancies();
});

function getAllStudents() {
	$('#studentTBody tr').remove();
	$.ajax({
		url:"/springhbex/getStudentList",
		type:"GET",
		success: function(data){
			console.log(data);
			for(var i in data) {
				var studentId = data[i].studentId;
				var name = data[i].name;
				var phNumber = data[i].phoneNumber;
				var alternativePhNumber = data[i].alternativePhoneNumber;
				var emailId = data[i].emailId;
				var referredBy = data[i].referredBy;
				var comments = data[i].comments;
				$('#studentTBody').append('<tr><td>'+name+'</td><td>'+phNumber+'</td><td>'
						+alternativePhNumber+'</td><td>'+emailId+'</td><td>'+referredBy+'</td><td>'+comments+'</td></tr>');
			}
		}
	});
}

function getAllTrainers() {

	$.ajax({
		url:"/springhbex/getTrainerList",
		type:"GET",
		success: function(data){
			console.log(data);
			/*for(var i in data) {
				var studentId = data[i].studentId;
				var name = data[i].name;
				var phNumber = data[i].phoneNumber;
				var alternativePhNumber = data[i].alternativePhoneNumber;
				var emailId = data[i].emailId;
				var referredBy = data[i].referredBy;
				var comments = data[i].comments;
				$('#studentTBody').append('<tr><td>'+name+'</td><td>'+phNumber+'</td><td>'
						+alternativePhNumber+'</td><td>'+emailId+'</td><td>'+referredBy+'</td><td>'+comments+'</td></tr>');
			}*/
		}
	});
}

function getAllConsultancies() {
	$.ajax({
		url:"/springhbex/getConsultancyList",
		type:"GET",
		success: function(data){
			console.log(data);
			/*for(var i in data) {
				var studentId = data[i].studentId;
				var name = data[i].name;
				var phNumber = data[i].phoneNumber;
				var alternativePhNumber = data[i].alternativePhoneNumber;
				var emailId = data[i].emailId;
				var referredBy = data[i].referredBy;
				var comments = data[i].comments;
				$('#studentTBody').append('<tr><td>'+name+'</td><td>'+phNumber+'</td><td>'
						+alternativePhNumber+'</td><td>'+emailId+'</td><td>'+referredBy+'</td><td>'+comments+'</td></tr>');
			}*/
		}
	});
}