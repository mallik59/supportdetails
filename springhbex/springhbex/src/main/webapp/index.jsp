<!DOCTYPE html>
<html lang="en">
<head>
<title>Student Details</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/student.js"></script>
<style>
table, th, td {
    border: 1px solid black;
}
</style>
</head>
<body>

	<div class="container">
	<br/><br/>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#studentDetails">Home</a></li>
    <li><a data-toggle="tab" href="#trainerDetails">Trainers</a></li>
    <li><a data-toggle="tab" href="#consultancyDetails">Consultancies</a></li>
    <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
  </ul>

  <div class="tab-content">
    <div id="studentDetails" class="tab-pane fade in active">
	 	<h2>Student Details</h2>
		<div id="studentTableDiv">
			<table id="studentTable">
				<thead>
					<tr>
						<th>Student Name</th>
						<th>Phone Number</th>
						<th>Alternative Phone number</th>
						<th>Email Id</th>
						<th>Referred By</th>
						<th>Comments</th>
					</tr>
				</thead>
				<tbody id="studentTBody">
				</tbody>
			</table>
		</div>
    </div>
    <div id="trainerDetails" class="tab-pane fade">
    	<h2>Trainer Details</h2>
		<div id="trainerTableDiv">
			<table id="trainerTable">
				<thead>
					<tr>
						<th>Trainer Name</th>
						<th>Phone Number</th>
						<th>Alternative Phone number</th>
						<th>Email Id</th>
						<th>Referred By</th>
						<th>Comments</th>
					</tr>
				</thead>
				<tbody id="trainerTBody">
				</tbody>
			</table>
		</div>  
    </div>
    <div id="consultancyDetails" class="tab-pane fade">
		<h2>Consultancy Details</h2>
		<div id="consultancyTableDiv">
			<table id="consultancyTable">
				<thead>
					<tr>
						<th>Consultancy Name</th>
						<th>Phone Number</th>
						<th>Alternative Phone number</th>
						<th>Email Id</th>
						<th>Referred By</th>
						<th>Comments</th>
					</tr>
				</thead>
				<tbody id="consultancyTBody">
				</tbody>
			</table>
		</div>  
	</div>
    <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
  </div>
	
		<!-- <h2>Large Modal</h2>
		Trigger the modal with a button
		<button type="button" class="btn btn-info btn-lg" data-toggle="modal"
			data-target="#myModal">Open Large Modal</button>

		Modal
		<div class="modal fade" id="myModal" role="dialog"
			data-backdrop="static" data-keyboard=false>
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Modal Header</h4>
					</div>
					<div class="modal-body">
						<p>This is a large modal.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div> -->
	</div>
</body>
</html>
