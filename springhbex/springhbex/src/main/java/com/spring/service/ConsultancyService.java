package com.spring.service;

import java.util.List;

import com.spring.bean.Consultancy;

public interface ConsultancyService {
	boolean saveConsultancy(Consultancy consultancy);

	List<Consultancy> consultancyList();
}
