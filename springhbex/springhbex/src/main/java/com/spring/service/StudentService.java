package com.spring.service;

import java.util.List;

import com.spring.bean.Student;

public interface StudentService {
	boolean saveStudent(Student student);

	List<Student> studentList();
}
