package com.spring.service;

import java.util.List;

import com.spring.bean.Trainer;

public interface TrainerService {
	boolean saveTrainer(Trainer trainer);
	List<Trainer> trainerList();
}
