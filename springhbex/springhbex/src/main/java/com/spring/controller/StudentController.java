package com.spring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.bean.Student;
import com.spring.dto.StudentDTO;
import com.spring.service.StudentService;

@Controller
public class StudentController {

	@Autowired
	private StudentService studentService;

	@RequestMapping(value="/getStudentList",method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody List<Student> getStudentsList(HttpServletRequest request,HttpServletResponse response) {
		List<Student> studentList = studentService.studentList();
		return studentList;
	}
	
	
	@RequestMapping(value = "/saveStudent", method = RequestMethod.POST)
	@ResponseBody
	public String saveStudent(HttpServletRequest request, HttpServletResponse response, StudentDTO studentDTO) {
		Student student = new Student();
		student.setName(studentDTO.getName());
		student.setEmailId(studentDTO.getEmailId());
		student.setPhoneNumber(studentDTO.getPhoneNumber());
		student.setAlternativePhoneNumber(studentDTO.getAlternativePhoneNumber());
		student.setReferredBy(studentDTO.getReferredBy());
		student.setComments(studentDTO.getComments());
		System.out.println(student);
		String message = "";
		boolean isStudentSaved = false;
		isStudentSaved = studentService.saveStudent(student);
		if (isStudentSaved) {
			message = "Student Details Successfully Saved";
		} else {
			message = "Failed to save student";
		}
		return message;
	}
	
}
