package com.spring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.bean.Consultancy;
import com.spring.service.ConsultancyService;

@Controller
public class ConsultancyController {
	
	@Autowired
	private ConsultancyService consultancyService;
	
	@RequestMapping(value="/getConsultancyList",method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody List<Consultancy> getStudentsList(HttpServletRequest request,HttpServletResponse response) {
		List<Consultancy> consultancyList = consultancyService.consultancyList();
		return consultancyList;
	}
	
}
