package com.spring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.bean.Trainer;
import com.spring.service.TrainerService;

@Controller
public class TrainerController {

	@Autowired
	private TrainerService trainerService;
	
	@RequestMapping(value="/getTrainerList",method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody List<Trainer> getStudentsList(HttpServletRequest request,HttpServletResponse response) {
		List<Trainer> trainerList = trainerService.trainerList();
		return trainerList;
	}
}
