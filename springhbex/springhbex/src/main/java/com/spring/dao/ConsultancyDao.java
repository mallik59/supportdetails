package com.spring.dao;

import java.util.List;

import com.spring.bean.Consultancy;

public interface ConsultancyDao {
	boolean saveConsultancy(Consultancy consultancy);

	List<Consultancy> consultancyList();
}
