package com.spring.dao;

import java.util.List;

import com.spring.bean.Student;

public interface StudentDao {
	boolean saveStudent(Student student);

	List<Student> studentList();
}
