package com.spring.dao;

import java.util.List;

import com.spring.bean.Trainer;

public interface TrainerDao {
	boolean saveTrainer(Trainer trainer);
	List<Trainer> trainerList();
}
