package com.spring.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "consultancy")
public class Consultancy {
	@Id
	@Column(name = "consultancy_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer consultancyId;
	@Column(name = "name")
	private String name;
	@Column(name = "phone_number")
	private long phoneNumber;
	@Column(name = "alternative_phone_number")
	private long alternativePhoneNumber;
	@Column(name = "email_id")
	private String emailId;
	@Column(name = "address")
	private String address;
	//@Column(name = "comments")
	transient String comments;

	public Integer getConsultancyId() {
		return consultancyId;
	}

	public void setConsultancyId(Integer consultancyId) {
		this.consultancyId = consultancyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public long getAlternativePhoneNumber() {
		return alternativePhoneNumber;
	}

	public void setAlternativePhoneNumber(long alternativePhoneNumber) {
		this.alternativePhoneNumber = alternativePhoneNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
