package com.spring.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.bean.Trainer;
import com.spring.dao.TrainerDao;

@Transactional
@Repository
public class TrainerDaoImpl implements TrainerDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Trainer> trainerList() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Trainer.class);
		criteria.addOrder(Order.asc("trainerId"));
		List<Trainer> trainerList = criteria.list();
		return trainerList;
	}

	@Override
	public boolean saveTrainer(Trainer trainer) {
		boolean isTrainerSaved = false;
		Session session = sessionFactory.getCurrentSession();
		try {
			session.save(trainer);
			isTrainerSaved = true;
		} catch (Exception e) {
			isTrainerSaved = false;
			e.printStackTrace();
		}
		return isTrainerSaved;
	}

}
