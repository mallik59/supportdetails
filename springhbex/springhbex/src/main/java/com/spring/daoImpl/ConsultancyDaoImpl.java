package com.spring.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.bean.Consultancy;
import com.spring.dao.ConsultancyDao;

@Transactional
@Repository
public class ConsultancyDaoImpl implements ConsultancyDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean saveConsultancy(Consultancy consultancy) {
		boolean isConsultancySaved = false;
		Session session = sessionFactory.getCurrentSession();
		try {
			session.save(consultancy);
			isConsultancySaved = true;
		} catch (Exception e) {
			isConsultancySaved = false;
			e.printStackTrace();
		}
		return isConsultancySaved;
	}

	@Override
	public List<Consultancy> consultancyList() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Consultancy.class);
		criteria.addOrder(Order.asc("consultancyId"));
		List<Consultancy> consultancyList = criteria.list();
		return consultancyList;
	}

}
