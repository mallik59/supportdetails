package com.spring.daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.bean.Student;
import com.spring.dao.StudentDao;

@Repository
public class StudentDaoImpl implements StudentDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean saveStudent(Student student) {
		boolean isStudentSaved = false;
		Session session = sessionFactory.getCurrentSession();
		try {
			session.save(student);
			isStudentSaved = true;
		} catch (Exception e) {
			isStudentSaved = false;
			e.printStackTrace();
		}
		return isStudentSaved;
	}

	@Override
	public List<Student> studentList() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Student.class);
		criteria.addOrder(Order.asc("studentId"));
		List<Student> studentList = criteria.list();
		return studentList;
	}

}
