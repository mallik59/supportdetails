package com.spring.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.bean.Student;
import com.spring.dao.StudentDao;
import com.spring.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentDao studentDao;

	@Override
	public boolean saveStudent(Student student) {
		return studentDao.saveStudent(student);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Student> studentList() {
		return studentDao.studentList();
	}

}
