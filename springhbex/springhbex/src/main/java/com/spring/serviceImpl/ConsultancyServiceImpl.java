package com.spring.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.bean.Consultancy;
import com.spring.dao.ConsultancyDao;
import com.spring.service.ConsultancyService;

@Service
public class ConsultancyServiceImpl implements ConsultancyService {

	@Autowired
	private ConsultancyDao consultancyDao;

	@Override
	public boolean saveConsultancy(Consultancy consultancy) {
		return consultancyDao.saveConsultancy(consultancy);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Consultancy> consultancyList() {
		return consultancyDao.consultancyList();
	}

}
