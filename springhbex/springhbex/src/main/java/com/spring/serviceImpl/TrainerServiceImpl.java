package com.spring.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.bean.Trainer;
import com.spring.dao.TrainerDao;
import com.spring.service.TrainerService;

@Service
public class TrainerServiceImpl implements TrainerService {

	@Autowired
	private TrainerDao trainerDao;
	
	@Transactional(readOnly = true)
	@Override
	public List<Trainer> trainerList() {
		return trainerDao.trainerList();
	}

	@Override
	public boolean saveTrainer(Trainer trainer) {
		return trainerDao.saveTrainer(trainer);
	}

}
